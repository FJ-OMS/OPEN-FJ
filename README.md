# skyer技术中台 技术底座 spring cloud微服务

#### 介绍

    SKYER技术中台 技术底座 spring cloud微服务-面对市场快速的变化和越来越繁琐的开发任务，我们需要技术底座来管理服务并解决已有的技术难点还要避免重复造轮子。系统支持通用的服务包括多租户SAAS模式、工作流、文件管理、消息应用、报表管理、分布式调度、通用导入以及告警管理等，并集成服务包括服务注册、能力接口注册、客户端注册、接口多级授权、计量计费、执行\健康监控、服务编排等。

#### 软件架构

软件架构说明 

![输入图片说明](image/技术架构.png)


#### 特性

云原生

    基于分布部署和统一运管，以分布式云，以容器、微服务、DevOps等技术为基础建立技术产品体系

![输入图片说明](image/云原生.png)

开发

    集成GitLab的代码统一管理能力、支持多种类型的制品库管理能力、集成CI/CD的自动化流水线、内建的代码扫描与质量分析，全流程跟进开发过程

![输入图片说明](image/开发.png)

部署

    包括集群管理、环境管理、资源管理，支持集群与环境的统一管理和集中分配，支持自动化部署流水线与一键式的手工部署，高效灵活地支撑产品运维

![输入图片说明](image/部署.png)

测试

    包括测试用例管理、测试计划管理、测试执行管理、缺陷管理、 测试报告管理，支持手工测试并提供API测试、性能测试、流量 回归测试、UI测试等自动化测试能力，测试任务还可嵌入开发流 水线，持续测试有效保障产品质量

![输入图片说明](image/测试.png)

技术支持

    轻松支持springcloud和Istio两种微服务模式，可以轻松为已部署的服务创建带来负载均衡、端到端的身份验证、监视等功能

![输入图片说明](image/技术支持.png)

#### 联系方式

![输入图片说明](image/we.png) ![输入图片说明](image/gzh.png)


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
